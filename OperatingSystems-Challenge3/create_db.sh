#! /bin/bash

#codul de mai jos pentru a se selecta userii cu pid-ul > 1000

cat /etc/passwd > new_passwd.txt
uid_min=$(grep "^UID_MIN" /etc/login.defs | awk '{print $2;}')
uid_max=$(grep "^UID_MAX" /etc/login.defs | awk '{print $2;}')

cut -d':' -f1,3 new_passwd.txt > all_users.txt

awk -F':' -v "u_min=$uid_min" -v "u_max=$uid_max" '{if($2>u_min && $2<=u_max) print $1;}' all_users.txt > 1000_users.txt

echo -e "\nPentru testare, se considera fisierul useri2.txt care contine numele userilor si
criteriile. Pentru introducerea altor nume (din sistemul pe care se testeaza), si anume, din: "
cat 1000_users.txt


op1="yes"
op2="no"

echo "Continuati?: yes/no"
read option

if test $option = $op1
then
	vi useri2.txt
else
	echo "no"
	kill $1
	exit
fi

mysql -u root --password=1234 -e "create database monitor"

mysql -u root --password=1234 monitor << EOFMYSQL
create table USERS(nrcrt int unique, user varchar(20) unique, criteriu1 varchar(20), criteriu2 varchar(20), criteriu3 int, contor int, prag int);
EOFMYSQL

i=1;

while read line
do

	array=($line)

mysql -u root --password=1234 monitor << EOFMYSQL
	insert into USERS (nrcrt, user, criteriu1, criteriu2, criteriu3, contor, prag) values($i,'${array[0]}', '${array[1]}', '${array[2]}', '${array[3]}', '', '${array[4]}');

EOFMYSQL
	
i=$(($i+1))
done < useri2.txt


mysql -u root --password=1234 -D "monitor" -e "select * from USERS;"

