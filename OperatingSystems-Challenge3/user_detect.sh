#!/bin/bash

#Criteriu1: interval orar
#Criteriu2: Un user nu are voie sa stea logat mai mult T ore
#Criteriu3: Un user nu are voie sa introduca gresit parola mai mult de 3 ori
#in caz contrat, contorul asociat lui va fi incrementat cu 1


function verify_crit1(){

#echo 'Se verifica criteriul 1'

if test $ora_min -lt $ora_max 
then
	
		if test $ora_min -le $ora -a $ora -le $ora_max 
		then
			#echo 'Acces autorizat iesire 1'
			echo ''
		else
			#echo 'Acces neautorizat iesire 2'
mysql -u root --password=1234 -D "monitor" -e "update USERS set contor=contor+1 where user='$nume'"
		fi

else
	if test $ora -le "2359" -a $ora -ge $ora_min 
	then
		#echo 'Acces autorizat iesire 3'
		echo ''
	else
		if test $ora -ge "0000" -a $ora -le $ora_max
		then
			echo ''
			#echo 'Acces autorizat iesire 4'
		else
			#echo 'Acces neautorizat 5'
	mysql -u root --password=1234 -D "monitor" -e "update USERS set contor=contor+1 where user='$nume'"
		fi
	fi
fi


}



function verify_crit3(){

	#echo "Se verifica criteriul 3:"
	sudo lastb -x | grep $nume | grep tty > temp2.txt

	data_max=$(head -n 1 data1.txt | awk '{print $1,$2,$3,$4;}')	
	#echo 'Data referinta maxim: ' $data_max
	data_max=$(date -d "$data_max" +%Y%m%d%H%M)
	#echo $data_max

	data_min=$(tail -n 1 data1.txt | awk '{print $1,$2,$3,$4;}')	
	#echo 'Data referinta minim: ' $data_min
	data_min=$(date -d "$data_min" +%Y%m%d%H%M)
	#echo $data_min

wrong_pass=0

while read line
do
	data_x=$(echo $line | awk '{print $4,$5,$6,$7;}')
	#echo $data_x	
	data_x=$(date -d "$data_x" +%Y%m%d%H%M)
	#echo "data: " $data_x

	#echo "data min: " $data_min
	#echo "data max: " $data_max
	if test $data_x -le $data_max -a $data_x -ge $data_min
	then
		#echo "se aduna"
		wrong_pass=$(($wrong_pass+1))
	fi
		
done < temp2.txt

	#echo "wrong pass for " $nume ": " $wrong_pass "crit: " $criteriu3
	if test $wrong_pass -ge $criteriu3
	then
	incr=$(($wrong_pass/$criteriu3))
	#echo "increm: " $incr
	mysql -u root --password=1234 -D "monitor" -e "update USERS set contor=contor+$incr
 where user='$nume'"
	fi


}







function verify_crit(){

	last -x | grep $nume | grep tty > temp1.txt
	
	data_max=$(head -n 1 data1.txt | awk '{print $1,$2,$3,$4;}')	
	#echo 'Data referinta maxim: ' $data_max
	data_max=$(date -d "$data_max" +%Y%m%d%H%M)
	#echo $data_max

	data_min=$(tail -n 1 data1.txt | awk '{print $1,$2,$3,$4;}')	
	#echo 'Data referinta minim: ' $data_min
	data_min=$(date -d "$data_min" +%Y%m%d%H%M)
	#echo $data_min


while read line
do
	data_x=$(echo $line | awk '{print $4,$5,$6,$7;}')
	#echo $data_x	
	data_x=$(date -d "$data_x" +%Y%m%d%H%M)
	#echo -e $data_x

	ora=$(echo $line | awk '{print $7;}')
	ora=$(echo $ora | sed 's/://g')


	line2=$(echo $line | awk '{print $10;}')
	#echo $line2
	MAX=$(echo $line2| sed 's/://g' | tail -c +2 | head -c -2) 


	if test $data_x -le $data_max -a $data_x -ge $data_min
	then
		verify_crit1
		

		#echo -e "Se verifica crit 2: "
		#echo "crit2: " $criteriu2 "si max: " $MAX
		if test $MAX -ge $criteriu2
		then
			#echo $nume "a stat logat: " $MAX
			#echo 'Acces neautorizat, timp depasit'
			mysql -u root --password=1234 -D "monitor" -e "update USERS set 	contor=contor+1 where user='$nume'"
		else
			#echo $nume "a stat logat: " $MAX
			#echo 'Acces autorizat'
			echo ''
fi

		#echo -e "\n\n"
	fi
done < temp1.txt

}










# Creare fisier in care se vor afla datele intre care se realizeaza monitorizarea
#La primul pas, daca fisierul nu exista, se vor adauga data curenta si data ultimei linii rezultate din comanda last. La rulari viitoare, fisierul fiind deja prezent, in el vor exista data ultimei monitorizari si data curenta. Dupa fiecare monitorizare, fisierul data1.txt este updat-at cu datele corespunzatoare, daca se doreste acest lucru


#echo "PID: " $$
ok=$(ls | grep -c data1.txt)

if test $ok -eq 0
then
	#echo "nu exista fis"
	./create_db.sh $$
	date > data1.txt
	last -x | tail -n 3 | head -n 1 | cut -d' ' -f 8,9,11,12 >> data1.txt
else
	#echo "exista fis"
	data_max=$(head -n 1 data1.txt | awk '{print $1,$2,$3,$4;}')
	#echo $data_max
	date > data1.txt
	#echo $data_max >> data1.txt
fi

#
NR=`mysql -u root --password=1234 -D "monitor" -e "select count(*) from USERS";`
nr=$(echo $NR | cut -d' ' -f 2)

#nume="ana"
for i in $(seq 1 $nr)
do

	nume=$( echo `mysql -u root --password=1234 -D "monitor" -e "select user from USERS where nrcrt=$i";` | cut -d' ' -f 2)
	
	
	#echo "Se verifica userul " $nume
	criteriu1=$( echo `mysql -u root --password=1234 -D "monitor" -e "select criteriu1 from USERS where nrcrt=$i";` | cut -d' ' -f 2)

	#echo 'Criteriu1: ' $criteriu1
	ora_min=$(echo $criteriu1 | cut -d',' -f 1 | tail -c +2)
	ora_min=$(echo $ora_min | sed 's/://g')
	ora_max=$(echo $criteriu1 | cut -d',' -f 2 | head -c -2)
	ora_max=$(echo $ora_max | sed 's/://g')
	#echo 'ora min: ' $ora_min 'ora max: ' $ora_max

	criteriu2=$( echo `mysql -u root --password=1234 -D "monitor" -e "select criteriu2 from USERS where nrcrt=$i";` | cut -d' ' -f 2)
	criteriu2=$(echo $criteriu2 | sed 's/://g')
	#echo "Criteriu 2:" $criteriu2

	criteriu3=$( echo `mysql -u root --password=1234 -D "monitor" -e "select criteriu3 from USERS where nrcrt=$i";` | cut -d' ' -f 2)

	verify_crit
	
	verify_crit3


	val1=$( echo `mysql -u root --password=1234 -D "monitor" -e "select contor from USERS where nrcrt=$i";` | cut -d' ' -f 2)
	val2=$( echo `mysql -u root --password=1234 -D "monitor" -e "select prag from USERS where nrcrt=$i";` | cut -d' ' -f 2)

	if test $val1 -ge $val2
	then
		sudo usermod -L $nume
		mysql -u root --password=1234 -D "monitor" -e "select  * from USERS;"
		
		mysql -u root --password=1234 -D "monitor" -e "update USERS set contor=0 where nrcrt=$i;"
		
	fi

	echo -e '\n'
done


#daca scriptul este de tip serviciu, continutul bazei de date monitor se pastreaza, pentru
#monitorizari viitoare. Daca se doreste insa o noua testare a script-ului, se sterge baza de date.

op1="yes"
op2="no"

echo -e "Stergeti baza de date pt o noua rulare/verificare? yes/no\n"
read option

if test $option = $op1
then
	mysql -u root --password=1234 -D "monitor" -e "select * from USERS;
drop database monitor;"

	rm data1.txt
else
	if test $option = $op2
	then
	mysql -u root --password=1234 -D "monitor" -e "select * from USERS;"
	exit
	fi
fi

