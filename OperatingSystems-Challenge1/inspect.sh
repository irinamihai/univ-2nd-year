#!/bin/bash

nume=ppiduri.txt
max=0

ps -e -o ppid | tail -n +2 > $nume #fisierul nume contine toate ppid-urile

while test $max -le 200 #se cauta ppid-ul care apare de cele mai multe ori
do			#pana cand numarul maxim este mai mare cat o anumita 				#valore; Scanarea sistemului se face de mai multe ori
			#la un anumit interval de timp
	while read linie;
	do
		nr=$(grep -iw $linie $nume | wc -l)

		if test $nr -gt $max
		then
			max=$nr
			solutie=$linie
		fi
		
	done <$nume

	
	echo $solutie ' are nr max de copii: ' $max
	sleep 5
	time=$(($time+1))

	ps -e -o ppid | tail -n +2 > $nume

done


	#echo $solutie

	kill -STOP $solutie #se opreste procesul parent, pentru a-i putea fi gasiti si stersi intre timp copiii

	fis1=fis1.txt
	fis2=fis2.txt
	ps -e -o pid,ppid | tail -n +2 > $fis1

	grep -iw $solutie $fis1 | awk '{print $1;}' > $fis2 #in fisierul fis2 vor fi procesele copii, precum si procesul parent

	while read copil;
	do
		#echo 'moare ' $copil
		kill -9 $copil
	done < $fis2

	echo 'moare' $solutie
	kill -9 $solutie #verificare ca, intr-adevar, a fost omorat procesul parent

rm $nume
rm $fis1
rm $fis2







