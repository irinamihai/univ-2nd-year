#!/bin/bash

ls -d /home/irina/test/* > fisiere_test

while read line
do

	tip=$(file $line)

	director=$(echo $tip | grep -ic 'directory')

	if test $director -eq 1
	then
		echo $line 'e director'
		ls -d $line/* >> fisiere_test
	fi

done < fisiere_test
