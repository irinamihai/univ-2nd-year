#!/bin/bash

fis_conf=files_to_modify.txt

#se citeste calea catre directorul care contine fisiere de modificat/verificat
while read continut
do

ls -d $continut/* > $fis_conf

done < checker.conf

file1=fis_sum1.txt #aici se vor adauga, pe rand, caracteristicile fisierelor
nr=0

#functie de verificare a caracteristicilor fisierelor
function calculate_sum(){ 

	tip=$(file $line)
	director=$(echo $tip | grep -ic 'directory')
	
	#daca fisierul este de tip director, atunci calea catre fisierele
	#pe care le contine sunt adaugate in files_to_modify.txt
	if test $director -eq 1
	then
		if test $(($nr%2)) -eq 0 
		then
			#echo 'Se adauga'
			ls -d $line/* >> $fis_conf
		fi
		
	else
	
		sum_md5=$(openssl dgst -md5 $line)
		echo $sum_md5 > $file1

		access=$(stat -c%a $line)
		#echo 'Access: ' $access
		cuvv=$(echo $access | sed 's/ //g')
		cuv_final1=$(./shift.sh $cuvv;) #shift.sh cripteaza datele obtinute
		echo $cuv_final1 >> $file1
	
		group=$(ls -l $line | awk '{print $4;}')
		#echo 'Group: ' $group
		cuvv=$(echo $group | sed 's/ //g')
		cuv_final2=$(./shift.sh $cuvv;)
		echo $cuv_final2 >> $file1

		owner=$(ls -l $line | awk '{print $3;}')
		#echo 'Owner: ' $owner
		cuvv=$(echo $owner | sed 's/ //g')
		cuv_final3=$(./shift.sh $cuvv;)
		echo $cuv_final3 >> $file1

		size=$(stat -c%s $line)
		#echo 'Size: ' $size
		cuvv=$(echo $size | sed 's/ //g')
		cuv_final4=$(./shift.sh $cuvv;)
		echo $cuv_final4 >> $file1

		last_access=$(stat -c%x $line)
		#echo 'Last access: ' $last_access
		cuvv=$(echo $last_access | sed 's/ //g')
		cuv_final5=$(./shift.sh $cuvv;)
		echo $cuv_final5 >> $file1

		last_modif=$(stat -c%y $line)
		#echo 'Last modification: ' $last_modif
		cuvv=$(echo $last_modif | sed 's/ //g')
		cuv_final6=$(./shift.sh $cuvv;)
		echo $cuv_final6 >> $file1

		time_stamp=$(ls -l $line | awk '{print $5}')
		#echo 'Last modification: ' $time_stamp
		cuvv=$(echo $last_modif | sed 's/ //g')
		cuv_final6=$(./shift.sh $cuvv;)
		echo $cuv_final6 >> $file1

	fi


}

while read line
do

	echo $line
	calculate_sum
	
	#se calculeaza checksum-ul pentru fisierul care contine caracteristicile fisierelor
	sum0=$(openssl dgst -md5 $file1 | cut -d' ' -f 2)
	echo 'Suma 1:' $sum0
	nr=$(($nr+1))

	./modify.sh $line

	calculate_sum
	sumc=$(openssl dgst -md5 $file1  | cut -d' ' -f 2)
	echo 'Suma 2:' $sumc	
	nr=$(($nr+1))

	if test $sum0 = $sumc
	then
		echo 'Nu a avut loc nicio modificare.'
	else
		echo 'Au avut loc modificari sau au fost accesate fisierele.'
	fi

	echo
	echo

done < $fis_conf

