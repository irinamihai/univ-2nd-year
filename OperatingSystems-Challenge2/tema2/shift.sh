#!/bin/bash
#se roteste cuv de rot ori, unde rot este restul impartirii la 10
#a valorii ASCII a primului caracter

cuv=$1
lit=$(echo $cuv | head -c 1)

rot=$(printf "%d" "'${lit}")
rot=$(($rot%10))

for i in $(seq 1 $rot)
do

	x=$(echo $cuv | head -c 1)
	y=$(echo $cuv | tail -c +2)
	
	z=$y$x
	cuv=$z

done

echo $cuv
