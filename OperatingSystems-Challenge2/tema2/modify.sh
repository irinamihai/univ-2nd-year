#!/bin/bash

line=$1
#se efectueaza modificari in functie de tipul fisierului

	ok=0
	tip=$(file $line)
	
	director=$(echo $tip | grep -ic 'directory')
		if test $director -eq 1
		then
			echo 'Director'
			ok=1
		fi

	shared=$(echo $tip | grep -ic 'shared')
		if test $shared -eq 1
		then
			echo 'Shared file'
			sudo chmod 711 $line
			sudo chown irina $line	
			ok=1
		fi

	execut=$(echo $tip | grep -ic 'executable')
		if test $execut -eq 1
		then
			echo 'Executabil, do nothing.'	
			ok=1
		fi

	char_special=$(echo $tip | grep -ic 'character special')
		if test $char_special -eq 1
		then
			echo 'Character Special'
			sudo chgrp scoala $line
			ok=1
		fi

	ordinary=$(echo $tip | grep -ic 'ASCII')
		if test $ordinary -eq 1
		then
			echo 'Ordinary file' > $line
			sudo chown ana $line
			sudo chgrp projects $line
			sudo chmod 765 $line
			ok=1
		fi
	
	if test $ok -eq 0
	then
		ok=0
		sudo touch -t 201203031359 $line
	fi
