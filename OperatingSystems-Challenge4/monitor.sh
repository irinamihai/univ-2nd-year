#!/bin/bash

# Monitor network stations

#informatiile sunt dispuse in fisierele conf1.txt si warning.txt
#scriptul este automatizat sa ruleze o data la 2 minute

# info is kept in conf1.txt and warning.txt
# the script run every 2 minutes
conex=$(ifconfig | grep 'inet addr' | grep -cv '127.0.0.1')

if test $conex -eq 1
then

    # check if it's the 1st time we're running this
	ok=$(ls | grep -ci 'conf1.txt') #se cauta fisierul conf1.txt pentru a se afla daca este prima rulare sau nu

	#echo 'ok: ' $ok

	if test $ok -eq 1
	then
			cat conf1.txt > copie.txt
	fi

	netstat -ai | tail -n +3 | awk '{print $1,$NF;}' > conf1.txt

	#cat conf1.txt

	echo -e '\n Statiile active sunt:\n' >> conf1.txt
		my_ip=$(ifconfig | grep 'inet addr' | grep -v '127.0.0.1' | awk '{print $2;}' | cut -d':' -f2 | cut -d'.' -f 1,2,3,4)
		#echo 'My ip: ' $my_ip
		ip0=$(ifconfig | grep 'inet addr' | grep -v '127.0.0.1' | awk '{print $2;}' | cut -d':' -f2 | cut -d'.' -f 1,2,3)
		ip1=".0-254"
		ip=$ip0$ip1

		nmap -sP $ip > conf2.txt

		cat conf2.txt | sed '/^$/d' | head -n -1 | tail -n +2 | grep -v '$my_ip'> conf3.txt

		nr=1

		while read line
		do

		rest=$(($nr%2))

		if test $rest -eq 1
		then
			adr_ip=$(echo $line | awk '{print $2;}') 
			if test $adr_ip != $my_ip
			then
				nr=$(($nr+1))
			fi
		else
			nr=$(($nr+1))
			mac=$(echo $line | awk '{print $3;}')

        # Complete the rezultat.txt file
		#se completeaza fisierul rezultat.txt
			if test $ok -eq 0 
			then
				info_rez=$adr_ip" "$mac" ""0"
				echo $info_rez >> rezultat.txt
			else #daca exista deja fisierul conf1.txt, se cauta ip-ul curent
				gasit=$(cat copie.txt | grep -cw $adr_ip)

				if test $gasit -eq 0 #daca nu s-a gasit
				then
					#echo 'Nu s-a gasit ip-ul' 
					info_rez=$adr_ip" "$mac" ""0" #inseamna ca a statia e activa de 0 minute
				else #daca s-a gasit, inseamna ca statia e inca activa
					info_rez0=$(cat copie.txt | grep -w $adr_ip)
					info_rez=$(echo $info_rez0 | awk '{$NF=$NF+2; print;}')
				fi
				#echo 'Noua info: '$info_rez
				echo $info_rez >> rezultat.txt
			fi

		fi

		done < conf3.txt

		cat rezultat.txt >> conf1.txt
		rm rezultat.txt

else
	echo 'Nu exista conexiune!!! ' >> warning.txt
	date >> warning.txt
	echo -e '\n' >> warning.txt
fi

	

 
